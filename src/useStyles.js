import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles({
  root: {
    backgroundColor: "#F8F9FA",
    minHeight: "100vh",
    height: "100%",
    width: "100%",
  },
  sidebar: {
    width: "55px",
  },
  main: {
    width: "calc(100% - 55px)",
  },
  checkout: {
    color: "#fff",
    width: "168px",
    height: "38px",
    borderRadius: "4px",
    border: "none",
    fontWeight: "600",
    marginLeft: "15px",
    cursor: "pointer",
  },
  shoping: {
    padding: "10px 22px",
    bordeRadius: "4px",
    textTransform: "uppercase",
    fontSize: "14px",
    fontWeight: "600",
    color: "#f9fbfe",
  },
  container: {
    paddingRight: 40,
    paddingLeft: 40,
    boxSizing: "border-box",
    overflowX: "auto",
    "&::-webkit-scrollbar": {
      height: "5px",
    },
    "&::-webkit-scrollbar-track": {
      background: "transparent",
    },
    "&::-webkit-scrollbar-thumb": {
      borderRadius: "1.2px",
      border: "4px solid #dadbdd",
    },
  },
  delete: {
    marginLeft: "15px",
  },
  filterButton: {
    color: "#FFF",
    width: "100%",
    marginTop: "25px",
  },
  addButton: {
    color: "#fff",
    background: "#61d5df",
    padding: "7px",
    cursor: "pointer",
    marginRight: "15px",
    borderRadius: "5px",
  },
  sortIcon: {
    color: "#b6b9ca",
  },
  selectAllIcon: {
    height: "32px",
    width: "32px",
    display: "none",
    justifyContent: "center",
    alignItems: "center",
    padding: "5px 7px",
    marginLeft: "12px",
  },
  MenuIcon: {
    cursor: "pointer",
    color: "#61d5df",
    display: "none",
  },

  "@media only screen and (max-width: 1070px)": {
    sidebar: {
      width: "0px",
    },
    main: {
      width: "100%",
    },
  },
  "@media only screen and (max-width: 1070px)": {
    selectAllIcon: {
      display: "flex",
    },
  },
  "@media only screen and (max-width: 1000px)": {
    HelpOutlineIcon: {
      display: "none",
    },
    MenuIcon: {
      display: "block",
    },
    helpMenu: {
      display: "none",
    },
  },
  "@media only screen and (max-width: 970px)": {
    sidebar: {
      width: "0px",
    },
    main: {
      width: "100%",
    },
    addButton: {
      padding: "2px",
    },
  },
  "@media only screen and (max-width: 869px)": {
    burgerMenu: {
      display: "flex",
    },
  },
  "@media only screen and (max-width: 750px)": {
    shoping: {
      width: "100%",
    },
    checkout: {
      width: "100%",
      marginLeft: "0",
    },
  },
});
export default useStyles;

import { Button, TextField } from "@material-ui/core";
import "./UserForm.css";
import userImg from "../Img/profile-example.jpg";
import React, { useEffect } from "react";
import { useFormik } from "formik";
import { NavLink } from "react-router-dom";
import User from "../server/server";
import { useSnackbar } from "notistack";

const UserForm = () => {
  const { enqueueSnackbar } = useSnackbar();
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
    },
    onSubmit: (values) => {
      const user = JSON.parse(localStorage.getItem("user"));
      values.password
        ? handleUpdateUser(
            user.id,
            values.firstName,
            values.lastName,
            values.email,
            values.password
          )
        : handleUpdateUser(
            user.id,
            values.firstName,
            values.lastName,
            values.email
          );
    },
  });

  const handleUpdateUser = (id, firstName, lastName, email, password) => {
    User.updateUserDetails(id, firstName, lastName, email, password)
      .then((res) => {
        localStorage.setItem("user", JSON.stringify(res));
        alert("yey");
      })
      .catch(() => alert("sos"));
  };

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    User.getSingleUser(user.id)
      .then((data) => {
        formik.setValues({
          firstName: data.data.firstName,
          lastName: data.data.lastName,
          email: data.data.email,
        });
      })
      .catch(() => {
        enqueueSnackbar(`Could not get user resource. Try again`, {
          variant: "error",
        });
      });
  }, []);

  return (
    <div className="user__wrapper">
      <div className="user__info">
        <div className="info__header">
          <ul className="header__list">
            <NavLink
              activeClassName="activeNavForm"
              to={"#"}
              className="form__list--itemLink"
            >
              PROFILE
            </NavLink>
            <NavLink to={"#"} className="form__list--itemLink">
              BILLING
            </NavLink>
            <NavLink to={"#"} className="form__list--itemLink">
              INVOICE HISTORY
            </NavLink>
          </ul>
          <div className="header__deactivation"> deactivate account </div>
        </div>
        <form className="formik__form" onSubmit={formik.handleSubmit}>
          <div className="info__details">
            <div className="details__item">
              <div className="left__photo">
                <div className="photo__det">
                  <span className="photo__det-info">PROFILE PICTURE</span>
                  <div className="test">
                    <img src={userImg} alt="profilePic" />
                    <Button variant="contained" color="primary">
                      Upload
                    </Button>
                  </div>
                </div>
                <div className="photo__password">
                  <span className="photo__det-info">CHANGE PASSWORD</span>
                  <div className="test">
                    <TextField
                      type="text"
                      label="Current Password"
                      variant="outlined"
                      fullWidth
                    />
                    <TextField
                      type="text"
                      label="New Password"
                      variant="outlined"
                      fullWidth
                      id="password"
                      name="password"
                      value={formik.values.password}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.password &&
                        Boolean(formik.errors.password)
                      }
                      helperText={
                        formik.touched.password && formik.errors.password
                      }
                    />
                    <TextField
                      type="text"
                      label="Confirm New Password"
                      variant="outlined"
                      fullWidth
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="details__item">
              <div className="right__det">
                <div className="det__personal">
                  <span className="photo__det-info">Personal Details</span>
                  <div className="test">
                    <TextField
                      type="text"
                      label="First Name"
                      variant="outlined"
                      fullWidth
                      id="firstName"
                      name="firstName"
                      value={formik.values.firstName}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.firstName &&
                        Boolean(formik.errors.firstName)
                      }
                      helperText={
                        formik.touched.firstName && formik.errors.firstName
                      }
                    />
                    <TextField
                      type="text"
                      label="Last Name"
                      variant="outlined"
                      fullWidth
                      id="lastName"
                      name="lastName"
                      value={formik.values.lastName}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.lastName &&
                        Boolean(formik.errors.lastName)
                      }
                      helperText={
                        formik.touched.lastName && formik.errors.lastName
                      }
                    />
                    <TextField
                      type="text"
                      label="Country"
                      variant="outlined"
                      fullWidth
                    />
                  </div>
                </div>
                <div className="det__contact">
                  <span className="photo__det-info">Contact Information</span>
                  <div className="test">
                    <TextField
                      type="email"
                      label="Email"
                      variant="outlined"
                      fullWidth
                      id="email"
                      name="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.email && Boolean(formik.errors.email)
                      }
                      helperText={formik.touched.email && formik.errors.email}
                    />
                    <TextField
                      type="text"
                      label="Skype"
                      variant="outlined"
                      fullWidth
                    />
                    <TextField
                      type="text"
                      label="Phone"
                      variant="outlined"
                      fullWidth
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="info__btn">
            <Button type="submit" variant="contained" color="primary">
              Save Changes
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default UserForm;

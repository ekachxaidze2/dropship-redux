import HelpOutlineOutlinedIcon from "@material-ui/icons/HelpOutlineOutlined";
import { useHistory } from "react-router-dom";
import { SideMenu } from "../header/SideBarMenu";
import useStyles from "../useStyles";
import "./UserHeader.css";
const UserHeader = () => {
  const history = useHistory();
  const classes = useStyles();
  const logOut = () => {
    localStorage.removeItem("token");
    history.push("./login");
  };
  return (
    <div className="user__header">
      <span>My Profile</span>
      <div className="header__btn">
        <div onClick={logOut} className="btn__signout">
          SIGN OUT
        </div>
        <SideMenu />
        <HelpOutlineOutlinedIcon
          className={classes.helpMenu}
          fontSize="large"
          color="secondary"
        />
      </div>
    </div>
  );
};

export default UserHeader;

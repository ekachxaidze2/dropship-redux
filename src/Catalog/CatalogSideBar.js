import "./CatalogSideBar.css";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import Range from "./Range";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const CatSidebar = (props) => {
  return (
    <section className="aside__item">
      <div className="choose__item choose__item--top">
        <span>Choose Niche</span>
        <ExpandMoreIcon />
      </div>
      <div className="choose__item choose__item--bottom">
        <span>Choose Niche</span>
        <ExpandMoreIcon />
      </div>
      <div className="choose__options-wrapper">
        <div className="choose__options">
          Ship From
          <KeyboardArrowDownIcon />
        </div>
        <div className="choose__options">
          Ship To
          <KeyboardArrowDownIcon />
        </div>
        <div className="choose__options">
          select Supplier
          <KeyboardArrowDownIcon />
        </div>
      </div>
      <div className="range__slider">
        <Range />
      </div>
    </section>
  );
};

export default CatSidebar;

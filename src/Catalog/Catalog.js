import { useSelector } from "react-redux";
import "./Catalog.css";
import CatalogHead from "./CatalogHead";

function Catalog({
  image,
  title,
  price,
  handleSelect,
  id,
  selected,
  handleOpen,
  item,
  qty,
  showDelete,
  handleAddToCart,
  handleDeleteFromCart,
  checked,
}) {
  const selectedProducts = useSelector(
    (state) => state.product.selectedProducts
  );
  const checkboxClicked = () => {
    handleSelect(id);
  };

  const productClicked = () => {
    handleOpen({ id, image, title, price });
  };
  return (
    <div
      className={`catalog__product  ${
        selectedProducts.includes(item.id) ? "catalog__product--border" : ""
      }`}
    >
      <CatalogHead
        handleAddToCart={handleAddToCart}
        handleDeleteFromCart={handleDeleteFromCart}
        catalogSelected={selected}
        handleCatalogSelect={checkboxClicked}
        item={item}
        showDelete={showDelete}
        checked={checked}
      />
      <div onClick={productClicked} className="catalog__product-body">
        <div className="catalog__img">
          <img className="product__img" src={image} alt="" />
        </div>

        <div className="product__item--middle">
          <div className="catalog__title">{title}</div>
          <p className="catalog__supplier">
            By:
            <span className="catalog__supplier-identity">SP-Supplier115</span>
          </p>
        </div>
        <div className="product__item--price">
          <div className="catalog__prices">
            <div className="item__price">${price}</div>
            <div className="rrp">RRP</div>
          </div>
          <div className="divider"></div>
          <div className="catalog__prices">
            <div className="item__price">$64</div>
            <div className="rrp">Cost</div>
          </div>
          <div className="divider"></div>
          <div className="catalog__prices">
            <div className="item__price price__profit">14%($4)</div>
            <div className="rrp">/Profit</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Catalog;

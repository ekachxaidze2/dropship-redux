import "./CatalogHead.css";
import Checkbox from "./Checkbox";
import Button from "../header/SelectButton";
import { useDispatch, useSelector } from "react-redux";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { getProducts } from "../actions/productActions";
import { removeProduct } from "../server/server";
import { useIsAdmin } from "../hooks/useIsAdmin";
import { useState } from "react";
import ModifyProductModal from "../ModifyProductModal/ModifyProductModal";
import { useSnackbar } from "notistack";
import useStyles from "../useStyles";

const CatalogHead = ({
  handleCatalogSelect,
  item,
  handleAddToCart,
  checked,
}) => {
  const selectedProducts = useSelector(
    (state) => state.product.selectedProducts
  );
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const isAdmin = useIsAdmin();
  const [openEditModal, setOpenEditModal] = useState(false);
  const handleEdit = () => {
    setOpenEditModal(true);
  };
  const handleClose = () => {
    setOpenEditModal(false);
  };
  const handleDelete = () => {
    removeProduct(item.id)
      .then((res) => {
        enqueueSnackbar("Product has been removed successfully", {
          variant: "success",
        });
        dispatch(getProducts());
      })
      .catch((err) => {
        enqueueSnackbar("Could not remove product. Try again", {
          variant: "error",
        });
      });
  };

  return (
    <div
      className={`catalog__head 
        ${selectedProducts.includes(item.id) ? "catalog__head--active" : ""}`}
    >
      <Checkbox checked={checked} handleCheckboxChange={handleCatalogSelect} />
      <div>
        {isAdmin && (
          <>
            <EditIcon onClick={handleEdit} color="secondary" />
            <DeleteForeverIcon
              onClick={handleDelete}
              className={classes.delete}
              color="secondary"
            />
          </>
        )}
      </div>
      <Button handleClick={handleAddToCart} title="add to inventory" />
      <ModifyProductModal
        data={item}
        isOpen={openEditModal}
        handleClose={handleClose}
      />
    </div>
  );
};

export default CatalogHead;

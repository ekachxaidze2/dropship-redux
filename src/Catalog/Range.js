import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./Range.css";
import Slider from "@material-ui/core/Slider";
import { filterProducts } from "../actions/productActions";
import { Button } from "@material-ui/core";
import useStyles from "../useStyles";

const Range = () => {
  const [value, setValue] = useState([0, 5000]);
  const allProducts = useSelector((state) => state.product.allProducts);
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const priceRangeFilter = (items) => {
    const filteredProducts = items.filter((product) => {
      return product.price >= value[0] && product.price <= value[1];
    });
    return filteredProducts;
  };

  const handleChangeCommited = () => {
    const filtered = priceRangeFilter(allProducts);
    dispatch(filterProducts(filtered));
  };

  const resetFilter = () => {
    setValue([0, 5000]);
    const filteredProducts = allProducts.filter((product) => {
      return product.price >= 0 && product.price <= 5000;
    });
    dispatch(filterProducts(filteredProducts));
  };
  return (
    <>
      <div className="choose__item-slider">
        <Slider
          style={{ width: "95%" }}
          value={value}
          onChange={handleChange}
          onChangeCommitted={handleChangeCommited}
          color="secondary"
          min={0}
          max={5000}
        />
      </div>
      <div className="range-values">
        <div className="range-values__item">
          <div className="value__item value__item--currency">$</div>
          <div className="value__item value__item--price">{value[0]}</div>
        </div>
        <div className="range-values__item">
          <div className="value__item value__item--currency">$</div>
          <div className="value__item value__item--price">{value[1]}</div>
        </div>
      </div>
      <Button
        className={classes.filterButton}
        variant="contained"
        color="primary"
        onClick={resetFilter}
      >
        reset filter
      </Button>
    </>
  );
};

export default Range;

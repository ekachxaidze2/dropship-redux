import React from "react";
import "./Dashboard.css";
import DashboardIcon from "@material-ui/icons/Dashboard";

export const Dashboard = () => {
  return (
    <div className="page__wrapper">
      <h2 className="dashboard">Dashboard</h2>
      <DashboardIcon color="secondary" />
    </div>
  );
};

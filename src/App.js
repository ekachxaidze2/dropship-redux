import React, { useState, useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import { Grid } from "@material-ui/core";
import { useLocation } from "react-router-dom";
import Main from "./Main/Main";
import SideBar from "./sidebar/SideBar";
import Cart from "./Cart/Cart";
import Login from "./authorization/LogIn";
import SignUp from "./authorization/SignUp";
import { LandingPage } from "./LandingPage/LandingPage";
import User from "./User/UserPanel";
import useStyles from "./useStyles";
import { Dashboard } from "./Dashboard/Dashboard";
import { Orders } from "./Orders/Orders";
import { Transactions } from "./Transactions/Transactions";
import { AllStores } from "./AllStores/AllStores";
import { InventoryItems } from "./InventoryItems/InventoryItems";

function App() {
  const classes = useStyles();
  const location = useLocation();

  const [renderSideBar, setRenderSideBar] = useState(false);

  useEffect(() => {
    if (
      location.pathname === "/" ||
      location.pathname === "/login" ||
      location.pathname === "/signup"
    ) {
      setRenderSideBar(false);
    } else {
      setRenderSideBar(true);
    }
  }, [location, renderSideBar]);
  return (
    <>
      <Grid container className={classes.root}>
        {renderSideBar && (
          <Grid item className={classes.sidebar}>
            <SideBar />
          </Grid>
        )}

        <Grid container item className={renderSideBar ? classes.main : ""}>
          <Switch>
            <Route exact path="/">
              <LandingPage />
            </Route>
            <Route path="/profile" component={User}></Route>
            <Route path="/dashboard" component={Dashboard}></Route>
            <Route path="/catalog/:param?">
              <Main />
            </Route>
            <Route path="/AdminPanel" component={InventoryItems}></Route>
            <Route path="/cart" component={Cart}></Route>
            <Route path="/order" component={Orders}></Route>
            <Route path="/transactions" component={Transactions}></Route>
            <Route path="/list" component={AllStores}></Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/signup" component={SignUp}></Route>
          </Switch>
        </Grid>
      </Grid>
    </>
  );
}
export default App;

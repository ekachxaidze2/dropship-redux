import "./User.css";
import UserHeader from "./UserHeader";
import UserForm from "./UserForm";

const User = () => {
  return (
    <div className="user">
      <UserHeader />
      <UserForm />
    </div>
  );
};
export default User;

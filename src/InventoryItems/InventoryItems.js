import React from "react";
import "./Inventory.css";
import InboxIcon from "@material-ui/icons/Inbox";

export const InventoryItems = () => {
  return (
    <div className="page__wrapper">
      <h2 className="inventory">Inventory</h2>
      <InboxIcon color="secondary" />
    </div>
  );
};

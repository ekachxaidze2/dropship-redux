import { useState } from "react";
import SearchIcon from "@material-ui/icons/Search";

import "./SearchInput.css";
import { useDispatch } from "react-redux";
import { searchProducts } from "../actions/productActions";

const SearchInput = ({ placeholder }) => {
  const [input, setInput] = useState("");
  const dispatch = useDispatch();

  const handleSearch = (e) => {
    e.preventDefault();
    dispatch(searchProducts(input));
  };

  return (
    <form className="header__form" onSubmit={handleSearch}>
      <input
        id="searchQuery"
        className="header__input"
        type="text"
        value={input}
        placeholder={placeholder}
        onChange={(e) => setInput(e.target.value)}
      />
      <button id="searchButton" className="header__searchbtn" type="submit">
        <SearchIcon />
      </button>
    </form>
  );
};
export default SearchInput;

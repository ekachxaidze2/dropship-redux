import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import useStyles from "../useStyles";

const HelpMenu = () => {
  const classes = useStyles();
  return <HelpOutlineIcon className={classes.HelpOutlineIcon} />;
};
export default HelpMenu;

import { useDispatch } from "react-redux";
import "./Sort.css";
import { sortProducts } from "../actions/productActions";
import SortIcon from "@material-ui/icons/Sort";
import useStyles from "../useStyles";

function Sort() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const fillCatalog = (e) => {
    e.preventDefault();
    dispatch(sortProducts(e.target.value));
  };

  return (
    <div className="sort-section">
      <SortIcon className={classes.sortIcon}/>
      <select id="sort" onChange={fillCatalog}>
        <option value="def">Sort By: New Arrivals</option>
        <option value="asc">Price: High To Low</option>
        <option value="desc">Price: Low To High</option>
        <option value="alpasc">Alphabetic: high To Low</option>
        <option value="alpdesc">Alphabetic: Low To Low</option>
      </select>
    </div>
  );
}

export default Sort;

import { NavLink } from "react-router-dom";
import "./NavLink.css";
import { NavLinkIcons } from "./NavLinkIcons";
import Icon from "@material-ui/core/Icon";
import profileimg from "../Img/profile-example.jpg";
const NavLinks = (props) => {
  return (
    <ul className="SideBar__list">
      <NavLink to="/profile" activeClassName="activeNav">
        <div className="SideBar__list--itemLink SideBar__list--itemLink-user">
          <img className="user__img" src={profileimg} alt="user logo" />
        </div>
      </NavLink>
      {NavLinkIcons.map((icon, index) => (
        <li key={index} className="SideBar__list--item">
          <NavLink
            to={`/${icon.path}`}
            className="SideBar__list--itemLink"
            activeClassName="activeNav"
          >
            <Icon className="iconolor">{icon.iconName}</Icon>
          </NavLink>
        </li>
      ))}
    </ul>
  );
};
export default NavLinks;

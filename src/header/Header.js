import "./Header.css";
import Button from "./SelectButton";
import HelpMenu from "./HelpMenu";
import SearchInput from "./SearchInput";
import { selectProduct } from "../actions/productActions";
import { useDispatch, useSelector } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import { useIsAdmin } from "../hooks/useIsAdmin";
import { useState } from "react";
import ModifyProductModal from "../ModifyProductModal/ModifyProductModal";
import useStyles from "../useStyles";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import MenuIcon from "@material-ui/icons/Menu";
import { SideMenu } from "./SideBarMenu";
import { useMediaQuery } from "@material-ui/core";

function Header({
  selectedCount,
  count,
  fetchedProducts,
  setData,
  handleClearAll,
  // handleSelectAll,
  setSearchValue,
}) {
  const product = useSelector((state) => state.product.products);
  const isAdmin = useIsAdmin();
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openAddModal, setOpenAddModal] = useState(false);
  const matches = useMediaQuery("(max-width:660px)");
  const selectAll = () => {
    dispatch(selectProduct(product.map((selected) => selected.id)));
    product.map((item) => (item.selected = true));
  };
  const clearAll = () => {
    dispatch(selectProduct([]));
    product.map((item) => (item.selected = false));
  };

  const handleSelectAllIcon = () => {
    if (Object.keys(selectProduct).length > 0) {
      clearAll();
    } else {
      selectAll();
    }
  };

  function handleAdd() {
    setOpenAddModal(true);
  }

  const handleClose = () => {
    setOpenAddModal(false);
  };
  return (
    <div className="header">
      <div className="header__item">
        <Button
          className="selectAllBtn"
          handleClick={selectAll}
          title="SELECT ALL"
        />
        <div className="header__quantity-wrapper">
          <span className="header__selection header__selection--visible">{`selected ${selectedCount} out of `}</span>
          <span className="header__selection--show">{`${count} products`}</span>
        </div>
        {selectedCount > 0 ? (
          <Button
            className="deleteAllBtn"
            handleClick={clearAll}
            title="CLEAR ALL"
          />
        ) : (
          ""
        )}
        <Button
          className={classes.selectAllIcon}
          handleClick={handleSelectAllIcon}
          color="primary"
          variant="contained"
          // onClick={handleSelectAll}
          title={<CheckCircleIcon />}
        ></Button>
      </div>
      <div className="header__item">
        <SearchInput
          placeholder={"search..."}
          fetchedProducts={fetchedProducts}
          setData={setData}
          setSearchValue={setSearchValue}
        />
        <Button title={matches ? "ADD" : "ADD TO INVENTORY"} big />
        {isAdmin && (
          <AddIcon className={classes.addButton} onClick={handleAdd} />
        )}
        <SideMenu />
        <HelpMenu />
      </div>
      <ModifyProductModal isOpen={openAddModal} handleClose={handleClose} />
    </div>
  );
}

export default Header;

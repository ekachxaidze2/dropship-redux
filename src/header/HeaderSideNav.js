import Logo from "./Logo";
import "./HeaderSideNav.css";
import { NavLink } from "react-router-dom";
import NavLinkOptions from "./NavLinkIcons";
import Icon from "@material-ui/core/Icon";
import profileimg from "../Img/profile-example.jpg";

const HeaderSideNav = () => {
  return (
    <div className="SideBar__nav-right">
      <Logo />
      <ul className="SideBar__list">
        <li className="SideBar__list--item-right">
          <NavLink
            className="SideBar__list--itemLink"
            activeClassName="active-side"
            to="/profile"
          >
            <div className="linkWrapper">
              <p className="sideBar__link">Profile</p>
              <div className="SideBar__list--itemLink-user">
                <img className="user__img" src={profileimg} alt="user logo" />
              </div>
            </div>
          </NavLink>
        </li>
        {NavLinkOptions.map((icon, index) => (
          <li key={index} className="SideBar__list--item-right">
            <NavLink
              to={`/${icon.path}`}
              className="SideBar__list--itemLink"
              activeClassName="active-side"
            >
              <div className="linkWrapper">
                <p className="sideBar__link">{icon.pageName}</p>
                <Icon className="iconColor">{icon.iconName}</Icon>
              </div>
            </NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default HeaderSideNav;

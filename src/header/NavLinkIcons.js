export const NavLinkIcons = [
  {
    iconName: "av_timer",
    path: "dashboard",
  },
  {
    iconName: "list",
    path: "catalog",
  },
  {
    iconName: "view_in_ar",
    path: "AdminPanel",
  },
  {
    iconName: "shopping_cart",
    path: "cart",
  },
  {
    iconName: "check_box",
    path: "order",
  },
  {
    iconName: "swap_horizontal_circle",
    path: "transactions",
  },
  {
    iconName: "view_list",
    path: "list",
  },
];

const NavLinkOptions = [
  {
    pageName: "Dashboard",
    iconName: "av_timer",
    path: "dashboard",
  },
  {
    pageName: "Catalog",
    iconName: "list",
    path: "catalog",
  },
  {
    pageName: "Inventory",
    iconName: "view_in_ar",
    path: "AdminPanel",
  },
  {
    pageName: "Cart",
    iconName: "shopping_cart",
    path: "cart",
  },
  {
    pageName: "Orders",
    iconName: "check_box",
    path: "order",
  },
  {
    pageName: "Transactions",
    iconName: "swap_horizontal_circle",
    path: "transactions",
  },
  {
    pageName: "Store List",
    iconName: "view_list",
    path: "list",
  },
];

export default NavLinkOptions;

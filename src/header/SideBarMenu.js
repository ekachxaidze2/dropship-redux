import React, { useState } from "react";
import "./SideBarMenu.css";
import { Drawer } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import HeaderSideNav from "./HeaderSideNav";
import useStyles from "../useStyles";

export const SideMenu = () => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const classes = useStyles();

  return (
    <div className="burger-menu">
      <MenuIcon
        onClick={() => {
          setDrawerOpen(true);
        }}
        className={classes.MenuIcon}
      />
      <Drawer
        open={drawerOpen}
        onClose={() => {
          setDrawerOpen(false);
        }}
        anchor="right"
      >
        <HeaderSideNav />
      </Drawer>
    </div>
  );
};

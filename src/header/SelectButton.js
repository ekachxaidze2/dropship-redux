import "./SelectButton.css";

const Button = (props) => {
  return (
    <button
      onClick={props.handleClick}
      type={props.type}
      className={
        `header__button` +
        (props.big ? " header__button--big" : "") +
        ` ${props.className}`
      }
    >
      {props.title}
    </button>
  );
};

export default Button;

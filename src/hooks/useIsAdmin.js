import {useEffect, useState} from "react";

export const useIsAdmin= () => {
  const [isAdmin, setIsAdmin] = useState(false)
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"))
    setIsAdmin(user.isAdmin)
  }, [isAdmin])
  return isAdmin
}

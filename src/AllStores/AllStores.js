import React from "react";
import "./AllStores.css";
import ListIcon from "@material-ui/icons/List";

export const AllStores = () => {
  return (
    <div className="page__wrapper">
      <h2 className="stores">Stores List</h2>
      <ListIcon color="secondary" />
    </div>
  );
};

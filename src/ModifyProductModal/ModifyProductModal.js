import "./ModifyProductModal.css";
import React, { useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";
import * as yup from "yup";
import { useFormik } from "formik";
import { useHistory } from "react-router-dom";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  InputAdornment,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import { addProduct, editProduct } from "../server/server";
import { getProducts } from "../actions/productActions";
import Button from "../header/SelectButton";
import { useSnackbar } from "notistack";
import useStyles from "../useStyles";

const productSchema = yup.object({
  title: yup
    .string()
    .required("Required")
    .min(2, "Title must exceed 2 characters"),
  description: yup
    .string()
    .required("Required")
    .min(4, "Description must exceed 4 characters"),
  price: yup.number().integer("Price must be an integer").required("Required"),
  imageUrl: yup.string().url("Input must be a url"),
});

const ModifyProductModal = ({ data, isOpen = false, handleClose }) => {
  const [open, setOpen] = useState(isOpen);
  const dispatch = useDispatch();
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const formik = useFormik({
    initialValues: {
      title: data?.title,
      description: data?.description,
      price: data?.price,
      imageUrl: data?.imageUrl,
      id: data?.id,
    },
    onSubmit: (values) => {
      submit(values);
    },
    validationSchema: productSchema,
  });
  useEffect(() => {
    setOpen(isOpen);
  }, [isOpen]);

  const submit = (values) => {
    if (data?.id) {
      editProduct(values)
        .then((res) => {
          enqueueSnackbar(`Product edited successfully.`, {
            variant: "success",
          });
          history.push(`/catalog`);
          setOpen(false);
          dispatch(getProducts());
        })
        .catch((err) => {
          enqueueSnackbar(`Could not update product. Try again`, {
            variant: "error",
          });
        });
    } else {
      addProduct(values)
        .then((res) => {
          enqueueSnackbar(`New product has been added successfully.`, {
            variant: "success",
          });
          history.push(`/catalog`);
          setOpen(false);
          dispatch(getProducts());
        })
        .catch((err) => {
          enqueueSnackbar(`Could not create new product. Try again`, {
            variant: "error",
          });
        });
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <form onSubmit={formik.handleSubmit} className="product-form">
        <DialogTitle id="form-dialog-title">
          {data?.id ? "Edit Product" : "Add Product"}
        </DialogTitle>
        <DialogContent>
          <TextField
            label="Product name"
            variant="outlined"
            name="title"
            id="title"
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.title && Boolean(formik.errors.title)}
            helperText={formik.touched.title && formik.errors.title}
            style={{ width: "100%", color: "grey", margin: "12px 0px" }}
            color="primary"
          />
          <TextField
            label="Description"
            variant="outlined"
            name="description"
            id="description"
            multiline
            rows={5}
            value={formik.values.description}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={
              formik.touched.description && Boolean(formik.errors.description)
            }
            helperText={formik.touched.description && formik.errors.description}
            style={{ width: "100%", color: "grey", margin: "12px 0px" }}
            color="primary"
          />
          <TextField
            label="Price"
            variant="outlined"
            type="number"
            name="price"
            id="price"
            value={formik.values.price}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.price && Boolean(formik.errors.price)}
            helperText={formik.touched.price && formik.errors.price}
            InputProps={{
              inputProps: {
                min: 0,
              },
              startAdornment: (
                <InputAdornment position="start">$</InputAdornment>
              ),
            }}
            style={{ width: "100%", color: "grey", margin: "12px 0px" }}
            color="primary"
          />
          <TextField
            label="Image url"
            variant="outlined"
            name="imageUrl"
            id="imageUrl"
            value={formik.values.imageUrl}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.imageUrl && Boolean(formik.errors.imageUrl)}
            helperText={formik.touched.imageUrl && formik.errors.imageUrl}
            style={{ width: "100%", color: "grey", margin: "12px 0px" }}
            color="primary"
          />
        </DialogContent>
        <DialogActions>
          <Button
            handleClick={handleClose}
            color="primary"
            title="Cancel"
            type="button"
          />
          <Button
            type="submit"
            color="primary"
            title={data?.id ? "Edit" : "Add"}
          />
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default ModifyProductModal;

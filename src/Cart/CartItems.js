import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import "./CartItems.css";
import {
  Paper,
  TableRow,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  Table,
} from "@material-ui/core";
import { removeFromCart, updateCartList } from "../actions/cartActions";
import QuantityQounter from "./QuantityQounter";
import DeleteIcon from "@material-ui/icons/Delete";
import { useSnackbar } from "notistack";
import useStyles from "../useStyles";

export default function CartList() {
  const classes = useStyles();
  const { cart } = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const quantity = (e, id) => {
    dispatch(updateCartList(id, e.currentTarget.value));
  };

  const remove = (id) => {
    dispatch(removeFromCart(id));
    enqueueSnackbar(`item removed from cart.`, { variant: "info" });
  };
  return (
    <TableContainer component={Paper} className={classes.container}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>
              <span className="diff">ITEM DESCRIPTION</span>
            </TableCell>
            <TableCell align="center">
              <span className="diff">SUPPLIER</span>
            </TableCell>
            <TableCell align="center">
              <span className="diff">QUANTITY</span>
            </TableCell>
            <TableCell align="center">
              <span className="diff">ITEM COST</span>
            </TableCell>
            <TableCell align="center">
              <span className="diff">DELETE</span>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody className={classes.tbody}>
          {cart &&
            cart.map(
              (item) =>
                item.qty > 0 && (
                  <TableRow key={item.title}>
                    <TableCell component="th">
                      <div className="cntr">
                        <img
                          className="table-img"
                          src={item.image}
                          alt="prodImg"
                        ></img>
                        <span className="diff title">{item.title}</span>
                      </div>
                    </TableCell>
                    <TableCell align="center">
                      <span className="supplier">SP-Supplier115</span>
                    </TableCell>
                    <TableCell align="left">
                      <div className="qty">
                        <QuantityQounter
                          qty={item.qty}
                          onChange={(e) => quantity(e, item.id)}
                        />
                      </div>
                    </TableCell>
                    <TableCell align="center">
                      <span className="cost"> {item.price * item.qty}$</span>
                    </TableCell>
                    <TableCell align="center">
                      <DeleteIcon onClick={() => remove(item.id)} />
                    </TableCell>
                  </TableRow>
                )
            )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

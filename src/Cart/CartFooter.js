import React from "react";
import Button from "@material-ui/core/Button";
import "./CartFooter.css";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import useStyles from "../useStyles";

const CartFooter = () => {
  const classes = useStyles();

  return (
    <div className="cart__footer">
      <div className="cart__checkout">
        <Button className={classes.shoping} variant="contained" color="primary">
          <KeyboardBackspaceIcon />
          continue shopping
        </Button>
      </div>
      <div className="footer__data">
        <div>
          BALANCE:<span className="price">$0</span>
        </div>
        <div>
          ITEMS TOTAL:<span className="price">$0</span>
        </div>
        <div>
          SHIPPING TOTAL:<span className="price">$0</span>
        </div>
        <div>
          ORDER TOTAL:<span className="price">$0</span>
        </div>
        <Button
          className={classes.checkout}
          variant="contained"
          color="primary"
        >
          CHECKOUT
        </Button>
      </div>
    </div>
  );
};
export default CartFooter;

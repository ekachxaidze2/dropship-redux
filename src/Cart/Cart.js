import "./Cart.css";
import CartHeader from "./CartHeader";
import CartFooter from "./CartFooter";
import CartItems from "./CartItems";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getCartList } from "../actions/cartActions";

const Cart = () => {
  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);

  useEffect(() => {
    dispatch(getCartList());
  }, []);

  return (
    <div className="cart">
      <CartHeader />
      <CartItems cart={cart} />
      <CartFooter />
    </div>
  );
};

export default Cart;

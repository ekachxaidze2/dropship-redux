import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./Main.css";
import Catalog from "../Catalog/Catalog";
import { CircularProgress, Grid, makeStyles } from "@material-ui/core";
import Header from "../header/Header";
import Sort from "../header/Sort";
import { getProducts, selectProduct } from "../actions/productActions";
import CatSidebar from "../Catalog/CatalogSideBar";
import Modal from "../Modal/Modal";
import { useHistory, useParams } from "react-router-dom";
import API from "../reducers/API";
import { addToCart } from "../actions/cartActions";
import { useSnackbar } from "notistack";
function Main() {
  const [singleProduct, setSingleProduct] = useState(null);
  const [qty, setQty] = useState(0);
  const { enqueueSnackbar } = useSnackbar();
  const products = useSelector((state) => state.product.products);
  const selectedProducts = useSelector(
    (state) => state.product.selectedProducts
  );
  const dispatch = useDispatch();
  const { param } = useParams();
  const classes = useStyles();
  useEffect(() => {
    dispatch(getProducts());
  }, []);

  const history = useHistory();

  const handleSelect = (id) => {
    const foundId = selectedProducts.find((item) => item === id);
    const foundItem = products.find((item) => item.id === id);

    if (foundId) {
      const filteredProducts = selectedProducts.filter((item) => item !== id);
      dispatch(selectProduct(filteredProducts));
      foundItem.selected = false;
    } else {
      dispatch(selectProduct([...selectedProducts, id]));
      foundItem.selected = true;
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      history.push("./login");
    }
  }, [localStorage.hasOwnProperty("token")]);

  useEffect(() => {
    const productApi = API(`api/v1/products/${param}`);
    param && productApi.get().then((item) => setSingleProduct(item.data.data));
  }, []);

  const handleOpen = (data) => {
    history.push(`/catalog/${data.id}`);
    setSingleProduct(data);
  };
  const handleClose = () => {
    setSingleProduct(null);
    setQty(0);
    history.push(`/catalog`);
  };

  const addCart = (productId, qty) => {
    dispatch(addToCart(productId, qty || 1))
      .then((res) => {
        enqueueSnackbar(`Product added to cart successfully`, {
          variant: "success",
        });
      })
      .catch((err) => {
        enqueueSnackbar(`Could not add product to cart. Try again`, {
          variant: "error",
        });
      });
  };
  const increment = () => {
    setQty(() => qty + 1);
  };
  const decrement = () => {
    setQty(() => qty - 1);
  };

  return (
    <>
      {/*{ ? (*/}
      {/*  <Grid container alignItems={"center"} justify={"center"}>*/}
      {/*    <CircularProgress color={"primary"} />*/}
      {/*  </Grid>*/}
      {/*) : (*/}
      <div className={classes.main}>
        <CatSidebar />

        <div item className="content">
          <Header
            selectedCount={selectedProducts.length}
            count={products.length}
          />
          <Sort />
          <Grid container className="catalog">
            {products.map((item) => (
              <Grid item xs={12} sm={6} md={6} lg={4} key={item.id}>
                <Catalog
                  handleSelect={handleSelect}
                  id={item.id}
                  key={item.id}
                  image={item.imageUrl}
                  title={item.title}
                  price={item.price}
                  handleOpen={() => handleOpen(item)}
                  selected={item.selected}
                  item={item}
                  checked={item.selected}
                  handleAddToCart={() => addCart(item.id)}
                />
              </Grid>
            ))}
          </Grid>
        </div>
        {singleProduct && (
          <Modal
            product={singleProduct}
            handleClose={handleClose}
            handleAddToCart={() => addCart(singleProduct.id, qty)}
            onIncrement={increment}
            onDecrement={decrement}
            qty={qty}
          />
        )}
      </div>
      {/*)}*/}
    </>
  );
}

const useStyles = makeStyles({
  main: {
    width: "100%",
    display: "flex",
  },
});

export default Main;

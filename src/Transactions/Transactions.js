import React from "react";
import "./Transactions.css";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";

export const Transactions = () => {
  return (
    <div className="page__wrapper">
      <h2 className="transactions">Transactions</h2>
      <MonetizationOnIcon color="secondary" />
    </div>
  );
};

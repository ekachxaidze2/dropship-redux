import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { applyMiddleware, compose, createStore } from "redux";
import combinedReducer from "./reducers";
import thunk from "redux-thunk";
import { createTheme, MuiThemeProvider } from "@material-ui/core";
import { SnackbarProvider } from 'notistack';


const composeEnchanser = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  combinedReducer,
  composeEnchanser(applyMiddleware(thunk))
);
const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 869,
      lg: 1280,
      xl: 1920,
    },
  },
  palette: {
    primary: {
      main: "#61D5DF",
    },
    secondary: {
      main: "#49547D",
    },
  },
});

ReactDOM.render(
  <Router>
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider maxSnack={3}>
      <Provider store={store}>
        <App />
      </Provider>
      </SnackbarProvider>
    </MuiThemeProvider>
  </Router>,
  document.getElementById("root")
);

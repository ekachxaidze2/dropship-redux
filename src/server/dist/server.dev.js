"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeProduct = exports.editProduct = exports.addProduct = exports.getSingleProduct = exports["default"] = exports.logIn = exports.signUp = void 0;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var SERVER_URL = "http://18.185.148.165:3000/";
var SERVER_URL_V1 = SERVER_URL + "api/v1/";

_axios["default"].interceptors.request.use(function (config) {
  config.headers.Authorization = "Bearer ".concat(localStorage.getItem("token"));
  return config;
});

var signUp = function signUp(firstName, lastName, email, password, passwordConfirmation) {
  var res;
  return regeneratorRuntime.async(function signUp$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(_axios["default"].post(SERVER_URL + "register", {
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            passwordConfirmation: passwordConfirmation
          }));

        case 3:
          res = _context.sent;
          localStorage.setItem("user", JSON.stringify(res.data.data));
          localStorage.setItem("token", res.data.data.token);
          _context.next = 11;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          throw new Error(_context.t0);

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 8]]);
};

exports.signUp = signUp;

var logIn = function logIn(email, password) {
  var res;
  return regeneratorRuntime.async(function logIn$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return regeneratorRuntime.awrap(_axios["default"].post(SERVER_URL + "login", {
            email: email,
            password: password
          }));

        case 3:
          res = _context2.sent;
          localStorage.setItem("user", JSON.stringify(res.data.data));
          localStorage.setItem("token", res.data.data.token);
          _context2.next = 11;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2["catch"](0);
          alert("erooooooor");

        case 11:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[0, 8]]);
};

exports.logIn = logIn;
var User = {
  getSingleUser: function getSingleUser(id) {
    var response;
    return regeneratorRuntime.async(function getSingleUser$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return regeneratorRuntime.awrap(_axios["default"].get(SERVER_URL + "api/v1/users/".concat(id)));

          case 3:
            response = _context3.sent;
            return _context3.abrupt("return", response.data);

          case 7:
            _context3.prev = 7;
            _context3.t0 = _context3["catch"](0);
            throw new Error(_context3.t0);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  updateUserDetails: function updateUserDetails(id, firstName, lastName, email, password) {
    var response;
    return regeneratorRuntime.async(function updateUserDetails$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return regeneratorRuntime.awrap(_axios["default"].put(SERVER_URL + "api/v1/users/".concat(id), {
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password
            }));

          case 3:
            response = _context4.sent;
            return _context4.abrupt("return", response.data);

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](0);
            alert("vera heh");

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[0, 7]]);
  }
};
var _default = User;
exports["default"] = _default;

var getSingleProduct = function getSingleProduct(id) {
  var res;
  return regeneratorRuntime.async(function getSingleProduct$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return regeneratorRuntime.awrap(_axios["default"].get(SERVER_URL_V1 + "products/".concat(id)));

        case 3:
          res = _context5.sent;
          return _context5.abrupt("return", res && res.data.data);

        case 7:
          _context5.prev = 7;
          _context5.t0 = _context5["catch"](0);
          throw new Error(_context5.t0);

        case 10:
        case "end":
          return _context5.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.getSingleProduct = getSingleProduct;

var addProduct = function addProduct(data) {
  var res;
  return regeneratorRuntime.async(function addProduct$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return regeneratorRuntime.awrap(_axios["default"].post(SERVER_URL_V1 + "products", data));

        case 3:
          res = _context6.sent;
          return _context6.abrupt("return", res.data.data);

        case 7:
          _context6.prev = 7;
          _context6.t0 = _context6["catch"](0);
          throw new Error(_context6.t0);

        case 10:
        case "end":
          return _context6.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.addProduct = addProduct;

var editProduct = function editProduct(data) {
  var res;
  return regeneratorRuntime.async(function editProduct$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return regeneratorRuntime.awrap(_axios["default"].put(SERVER_URL_V1 + "products/".concat(data.id), data));

        case 3:
          res = _context7.sent;
          return _context7.abrupt("return", res.data.data);

        case 7:
          _context7.prev = 7;
          _context7.t0 = _context7["catch"](0);
          throw new Error(_context7.t0);

        case 10:
        case "end":
          return _context7.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.editProduct = editProduct;

var removeProduct = function removeProduct(id) {
  var res;
  return regeneratorRuntime.async(function removeProduct$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          _context8.next = 3;
          return regeneratorRuntime.awrap(_axios["default"]["delete"](SERVER_URL_V1 + "products/".concat(id)));

        case 3:
          res = _context8.sent;
          return _context8.abrupt("return", res.data.data);

        case 7:
          _context8.prev = 7;
          _context8.t0 = _context8["catch"](0);
          throw new Error(_context8.t0);

        case 10:
        case "end":
          return _context8.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.removeProduct = removeProduct;
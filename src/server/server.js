import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER_URL_V1 = SERVER_URL + "api/v1/";

axios.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const signUp = async (
  firstName,
  lastName,
  email,
  password,
  passwordConfirmation
) => {
  try {
    const res = await axios.post(SERVER_URL + "register", {
      firstName,
      lastName,
      email,
      password,
      passwordConfirmation,
    });
    localStorage.setItem("user", JSON.stringify(res.data.data));
    localStorage.setItem("token", res.data.data.token);
  } catch (err) {
    throw new Error(err);
  }
};

export const logIn = async (email, password) => {
  try {
    const res = await axios.post(SERVER_URL + "login", {
      email,
      password,
    });
    localStorage.setItem("user", JSON.stringify(res.data.data));
    localStorage.setItem("token", res.data.data.token);
  } catch (err) {
    alert("erooooooor");
  }
};

const User = {
  getSingleUser: async (id) => {
    try {
      const response = await axios.get(SERVER_URL + `api/v1/users/${id}`);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
  updateUserDetails: async (id, firstName, lastName, email, password) => {
    try {
      const response = await axios.put(SERVER_URL + `api/v1/users/${id}`, {
        firstName,
        lastName,
        email,
        password,
      });
      return response.data;
    } catch (err) {
      alert("vera heh");
    }
  },
};

export default User;

export const getSingleProduct = async (id) => {
  try {
    const res = await axios.get(SERVER_URL_V1 + `products/${id}`);
    return res && res.data.data;
  } catch (err) {
    throw new Error(err);
  }
};

export const addProduct = async (data) => {
  try {
    const res = await axios.post(SERVER_URL_V1 + `products`, data);
    return res.data.data;
  } catch (err) {
    throw new Error(err);
  }
};

export const editProduct = async (data) => {
  try {
    const res = await axios.put(SERVER_URL_V1 + `products/${data.id}`, data);
    return res.data.data;
  } catch (err) {
    throw new Error(err);
  }
};

export const removeProduct = async (id) => {
  try {
    const res = await axios.delete(SERVER_URL_V1 + `products/${id}`);
    return res.data.data;
  } catch (err) {
    throw new Error(err);
  }
};

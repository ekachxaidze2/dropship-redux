import React from "react";
import "./Orders.css";
import LocalShippingIcon from "@material-ui/icons/LocalShipping";

export const Orders = () => {
  return (
    <div className="page__wrapper">
      <h2 className="orders">Orders</h2>
      <LocalShippingIcon color="secondary" />
    </div>
  );
};

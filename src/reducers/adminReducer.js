import { CHECK_STATUS } from "../actions/actionTypes"

const initialState = {
    isAdmin: false
}

const UserReducer = (state = initialState, action) => {
    switch(action.type) {
        case CHECK_STATUS: 
            return {
                ...state,
                isAdmin: action.payload.isAdmin
            }
        default: 
            return {...state}
    }
}

export default UserReducer
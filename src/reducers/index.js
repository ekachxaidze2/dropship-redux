import { combineReducers } from "redux";
import counterReducer from "./counterReducer";
import productReducer from "./productReducer";
import cartReducer from "./cartReducer";
import UserReducer from "./adminReducer";

const combinedReducer = combineReducers({
  counter: counterReducer,
  product: productReducer,
  cart: cartReducer,
  user: UserReducer,
});

export default combinedReducer;

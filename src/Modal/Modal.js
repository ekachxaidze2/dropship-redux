import React from "react";
import QuantityQounter from "../Cart/QuantityQounter";
import Button from "../header/SelectButton";
import "./Modal.css";
const Modal = ({
  product,
  handleClose,
  handleAddToCart,
  onIncrement,
  onDecrement,
  qty,
}) => {
  return (
    <div className="modal__container">
      <div className="modal__card">
        <div className="modal__close" onClick={handleClose}>
          ✖
        </div>
        <div className="modal__items">
          <div className="modal-item">
            <div className="item__info--price">
              <div className="about__price">
                <span className="about__pric-details">${product.price}</span>
                <span className="about__pric-mindetails">RRP</span>
              </div>
              <div className="about__price">
                <span className="about__pric-details">9$</span>
                <span className="about__pric-mindetails">COST</span>
              </div>
              <div className="about__price">
                <span className="about__pric-details">38% ($6)</span>
                <span className="about__pric-mindetails">PROFIT</span>
              </div>
            </div>
            <div className="item__info--image">
              <img
                className="item__image"
                src={product.imageUrl}
                alt="product"
              />
            </div>
          </div>
          <div className="modal-item">
            <div className="modal__aditional-info">
              <p>SKU# frgx-557645</p>
              <p>
                Supplier:<span>US-Supplier139</span>
              </p>
            </div>
            <h1 className="item__features--title">{product.title}</h1>
            <div className="modalShop__wrapper">
              <QuantityQounter
                onIncrement={onIncrement}
                onDecrement={onDecrement}
                qty={qty}
              />
              <Button
                handleClick={handleAddToCart}
                title="Add to My Inventory"
                big
              />
            </div>
            <h3 className="item__description-details">Product Details</h3>
            <p className="item__description">{product.description}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;

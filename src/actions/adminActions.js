import { CHECK_STATUS } from "./actionTypes"

export const checkStatus = (isAdmin) => {
    return {
        type: CHECK_STATUS,
        payload: { isAdmin }
    }
}